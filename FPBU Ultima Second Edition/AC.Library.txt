﻿'FUNCTIONS
script:Reload()
script:Unload()
'fileName()
'name(^)
'
script.chatCallback = "^"
script.createObjectCallback = "^"
script.deleteObjectCallback = "^"
script.drawCallback = "^"
script.gameEndCallback = "^"
script.keyCallback = "^"
script.mouseCallback = "^"
script.processChatCallback = "^"
script.processSpellCallback = "^"
script.statusCallback = "^"
script.timerCallback = {"^", }
'
'player:CalculateDamage(^, ) - in advlib
player:CalculateMagicDamage(^, )
player:GetInventorySlot(^)
player:Attack(^)
player:BuyItem(^)
player:UseSpell(^, )
player:HoldPosition()
player:LevelSpell(^)
player:MoveTo(^, z)
player:SellItem(^)
player:UseSpell(^, )
player:UseSpell(^,   )
player
':UseSpell(^, )
':GetSpell(^)
':Attack(^)
':GetBuff(^)
.buffCount()
':GetDistanceTo(^)
DrawCircle(^, x, y, z, 0xAARRGGBB)
'DrawText(^, Size, x, y, R, G, B, A, align) ' in advlib
ExitLoL()
GetCameraPosition()
GetCursorPosition()
GetGameState()
GetHero(^)
GetHeroByIndex(^)
GetHeroByNetworkId(^)
GetHeroCount()
GetLatency()
GetMousePosition()
GetObject(^)
GetObjectByIndex(^)
GetObjectByNetworkId(^)
GetObjectCount()
GetTick()
GetTimeElapsed()
IsChatOpen()
IsItemPurchasable(^)
IsRecipePurchasable(^)
IsScriptLoaded()
LoadScript(^.lua)
PingSignal(^, x, z, false)
PrintChat(^)
PrintFloatText(^, type, text)
SendChat(^)
Surrender()
UnloadScript(^.lua)
script:Unload()
'GLOBAL VALUES
'window
WINDOW_X
WINDOW_Y
WINDOW_W
WINDOW_H
'keyboard
KEY_DOWN
KEY_UP
'path
SCRIPT_PATH
'mouse
MOUSE_LDOWN
MOUSE_RDOWN
MOUSE_MDOWN
MOUSE_X1DOWN
MOUSE_X2DOWN
MOUSE_LUP
MOUSE_RUP
'ping types
PING_NORMAL
PING_FALLBACK
'team
TEAM_NONE
TEAM_NEUTRAL
TEAM_BLUE
TEAM_RED
TEAM_ENEMY
'spells
SPELL_SLOT_1 , _Q
SPELL_SLOT_2 , _W
SPELL_SLOT_3 , _E
SPELL_SLOT_4 , _R
'
SPELL_RECALL
'
ITEM_SLOT_1
ITEM_SLOT_2
ITEM_SLOT_3
ITEM_SLOT_4
ITEM_SLOT_5
ITEM_SLOT_6
'
SPELL_SUMMONER_1
SPELL_SUMMONER_2
'spell states
STATE_READY
STATE_NOTLEARNED
STATE_SUPRESSED
STATE_COOLDOWN
STATE_NOMANA
STATE_UNKNOWN
STATE_READY
'lanes
LANE_TOP
LANE_MID
LANE_BOT
LANE_NONE
'game
GAME_RUNNING
GAME_VICTORY
GAME_DEFEAT
'object status
STATUS_HEALTH
STATUS_MAXHEALTH
STATUS_MS
STATUS_MANA
'float text
FLOAT_RED1
FLOAT_RED2
FLOAT_GREEN1
FLOAT_GREEN2
FLOAT_PURPLE1
FLOAT_PURPLE2
FLOAT_YELLOW1
FLOAT_YELLOW2
FLOAT_YELLOW3
FLOAT_BLUE
FLOAT_LIGHTBLUE1
FLOAT_LIGHTBLUE2
FLOAT_GOLD
FLOAT_WHITE1
FLOAT_WHITE2
FLOAT_WHITE3
FLOAT_WHITEYELLOW
'draw text alignment
FONT_LEFT
FONT_CENTER
FONT_RIGHT
'starting tick
STARTED_TICK
'player obj
'player
'Script object
'script
'GAME OBJECTS
.position
.name
.visible
.valid
'creep camps
.active
.monstersKilled
'obj_AI_Hero
.networkId
.health
.maxHealth
.maxMana
.maxEnergy
.maxShield
.mana
.energy
.shield
.damage
.addDamage
.totalDamage
.magicArmor
.armor
.armorPen
.armorPenPercent
.magicPen
.magicPenPercent
.charName
.lane
.range
.team
.dead
.alias
.level
.ranged
.melee
.cdr
.buffCount
'player members
.addDamage
.alias
.armor
.armorPen
.armorPenPercent
.buffCount
.cdr
.ap
.ms
.charName
.controlled
.damage
.dead
.gold
.health
.lane
.level
.magicArmor
.magicPen
.magicPenPercent
.mana/energy/shield
.maxHealth
.maxMana/maxEnergy/maxShield
.melee
.name
.networkId
.position
.range
.ranged
.team
.totalDamage
.type
.valid
.visible
local
'LUA FUNCTIONS
assert
collectgarbage
dofile
error
getfenv
getmetatable
ipairs
load
loadfile
loadstring
module
next
pairs
pcall
print
rawequal
rawget
rawset
require
select
setfenv
setmetatable
tonumber
tostring
type
unpack
xpcall
coroutine.create
coroutine.resume
coroutine.running
coroutine.status
coroutine.wrap
coroutine.yield
debug.debug
debug.getfenv
debug.gethook
debug.getinfo
debug.getlocal
debug.getmetatable
debug.getregistry
debug.getupvalue
debug.setfenv
debug.sethook
debug.setlocal
debug.setmetatable
debug.setupvalue
debug.traceback
file:close
file:flush
file:lines
file:read
file:seek
file:setvbuf
file:write
io.close
io.flush
io.input
io.lines
io.open
io.output
io.popen
io.read
io.stderr
io.stdin
io.stdout
io.tmpfile
io.type
io.write
math.abs
math.acos
math.asin
math.atan
math.atan2
math.ceil
math.cos
math.cosh
math.deg
math.exp
math.floor
math.fmod
math.frexp
math.huge
math.ldexp
math.log
math.log10
math.max
math.min
math.modf
math.pi
math.pow
math.rad
math.random
math.randomseed
math.sin
math.sinh
math.sqrt
math.tan
math.tanh
os.clock
os.date
os.difftime
os.execute
os.exit
os.getenv
os.remove
os.rename
os.setlocale
os.time
os.tmpname
package.cpath
package.loaded
package.loaders
package.loadlib
package.path
package.preload
package.seeall
string.byte
string.char
string.dump
string.find
string.format
string.gmatch
string.gsub
string.len
string.lower
string.match
string.rep
string.reverse
string.sub
string.upper
table.concat
table.insert
table.maxn
table.remove
table.sort
'KEYWORDS
 for |function |end| if | then | do | loop | To | return | local | else | nil | elseif | true | false |